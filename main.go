package main

import (
	"os"

	"gitlab.com/martyn.andrw/task-l1/solutions"
)

func main() {
	task := os.Args[1]

	// просто вызов кода задач
	// в некоторых задачах есть несколько вариантов решения
	// в таком случае в качестве аргументов
	// нужно передавать <номер задачи>v<номер варианта>
	switch task {
	case "1":
		solutions.Task1()
	case "2":
		solutions.Task2()
	case "3":
		solutions.Task3()
	case "4v1":
		solutions.Task4V1()
	case "4v2":
		solutions.Task4V2()
	case "4v3":
		solutions.Task4V3()
	case "5v1":
		solutions.Task5V1()
	case "5v2":
		solutions.Task5V2()
	case "5v3":
		solutions.Task5V2()
	case "6":
		solutions.Task6()
	case "7":
		solutions.Task7()
	case "8":
		solutions.Task8()
	case "9":
		solutions.Task9()
	case "10":
		solutions.Task10()
	case "11":
		solutions.Task11()
	case "12":
		solutions.Task12()
	case "13":
		solutions.Task13()
	case "14":
		solutions.Task14()
	case "15":
		solutions.Task15()
	case "16":
		solutions.Task16()
	case "17":
		solutions.Task17()
	case "18":
		solutions.Task18()
	case "19":
		solutions.Task19()
	case "20":
		solutions.Task20()
	case "21":
		solutions.Task21()
	case "22":
		solutions.Task22()
	case "23":
		solutions.Task23()
	case "24":
		solutions.Task24()
	case "25":
		solutions.Task25()
	case "26":
		solutions.Task26()
	}
}
