package solutions

import (
	"fmt"
	"strings"
)

func reverseStr(str string) string {
	// использую руны, чтобы не повредить символы
	runes := []rune(str)
	size := len(runes)

	// просто идем до середины массива и меняемся с концом
	for i := 0; i < size/2; i++ {
		runes[i], runes[size-i-1] = runes[size-i-1], runes[i]
	}

	return string(runes)
}

func reverseStrV2(str string) string {
	// также можно использовать строки и разделение по пустой строке
	strs := strings.Split(str, "")
	size := len(strs)

	// самый эффективный способ конкатенации
	var res strings.Builder

	// просто идем с конца и кладем в строкого строителя
	for i := size - 1; i >= 0; i-- {
		res.WriteString(strs[i])
	}

	return res.String()
}

func testrs(str string) {
	fmt.Printf("%s : %s\n", str, reverseStr(str))
	fmt.Printf("%s : %s\n", str, reverseStrV2(str))
}

func Task19() {
	testrs("光栄のへナイラック")
	testrs("главрыба")
	testrs("топот")

	testrs("")
	testrs("a")
	testrs("ab")
	testrs("aba")
}
