package solutions

import (
	"fmt"
	"strings"
)

// I used map, because I need set
func allUnique(str string) bool {
	runes := []rune(strings.ToLower(str))
	setRunes := make(map[rune]struct{})

	for _, r := range runes {
		if _, exists := setRunes[r]; exists { // если этот символ уже использовался как ключ,
			return false // то функция возвращает false
		}
		setRunes[r] = struct{}{} // иначе добавит в мапу в качестве ключа
	}

	return true
}

func Task26() {
	fmt.Println(allUnique("aAaAaaA"))
	fmt.Println(allUnique("abcdeft"))
	fmt.Println(allUnique("главрыба"))
	fmt.Println(allUnique("ジョジョの奇妙な冒険"))
}
