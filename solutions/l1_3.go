package solutions

import (
	"context"
	"fmt"
	"sync"
)

// using constraint for generic from l1_2.go
// type Computable interface {
// 	constraints.Integer |
// 		constraints.Float |
// 		constraints.Complex
// }

// using channel
func ConcurencySumC[T Computable](array []T) T {
	ch := make(chan T) // makes new channel
	for _, el := range array {
		go func(ch chan T, el T) {
			ch <- el * el // writes to channel result
		}(ch, el)
	}

	var sum T
	for i := 0; i < len(array); i++ {
		sum += <-ch // reads from channel and adds to sum
	}
	return sum
}

// using mutex
func ConcurencySumM[T Computable](array []T) T {
	var mu sync.Mutex
	var wg sync.WaitGroup
	var sum T

	wg.Add(len(array))
	for _, el := range array {
		go func(el T, sum *T) {
			mu.Lock()       // using mutex just like in task 2
			*sum += el * el // changing value by pointer
			mu.Unlock()
			wg.Done()
		}(el, &sum)
	}
	wg.Wait()

	return sum
}

// using contex...
func ConcurencySumCtx[T Computable](array []T) T {
	var sum T
	var wg sync.WaitGroup

	// context contains pointer to sum
	ctx := context.WithValue(context.Background(), "sum", &sum)
	wg.Add(len(array))

	for _, el := range array {
		go func(ctx context.Context, el T) {
			s := ctx.Value("sum").(*T) // gets pointer to sum from context
			*s += el * el              // changes value in sum by pointer
			wg.Done()                  // notifies that g is done
		}(ctx, el)
	}

	wg.Wait()
	return sum
}

func Task3() {
	arri := []int{1, 2, 3, 4, 5}
	arrf := []float32{1.0, 2.2, 1.5}
	arrc := []complex64{2 + 4i, 1 + 1i}

	// channel
	fmt.Println(ConcurencySumC(arri))
	fmt.Println(ConcurencySumC(arrf))
	fmt.Println(ConcurencySumC(arrc))

	// mutex
	fmt.Println(ConcurencySumM(arri))
	fmt.Println(ConcurencySumM(arrf))
	fmt.Println(ConcurencySumM(arrc))

	// context - antipattern
	fmt.Println(ConcurencySumCtx(arri))
	fmt.Println(ConcurencySumCtx(arrf))
	fmt.Println(ConcurencySumCtx(arrc))
}
