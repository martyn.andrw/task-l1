package solutions

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"time"
)

// WaitGroup + closing channel
var wg sync.WaitGroup

// worker reads from channel
func workerV1(ch <-chan int, num int) {
	// reads until channel closes
	for el := range ch {
		fmt.Printf("Worker %d got: %d\n", num, el)
	}
	fmt.Printf("Worker %d has ended its work\n", num)
	wg.Done()
}

// main goroutine writes in channel
func Task4V1() {
	wc := configCount()
	ch := make(chan int)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, os.Interrupt)

	wg.Add(wc)
	for i := wc; i > 0; i-- {
		go workerV1(ch, i)
	}

	// endless loop for writing to channel
writeLoop:
	for {
		select {
		case <-sc: // if we receive an interrupt signal,
			close(ch)       // we close channel
			break writeLoop // and break loop
		case ch <- rand.Intn(100): // writes random num
			time.Sleep(100 * time.Millisecond) // not so fast
		}

	}
	fmt.Println("Writer has ended its work")
	wg.Wait()
}
