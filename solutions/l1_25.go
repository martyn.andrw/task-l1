package solutions

import (
	"context"
	"fmt"
	"time"
)

// функция сна с помощью time.After
func mySleepV1(timeout time.Duration) {
	<-time.After(timeout)
}

// с помощью с контекстов
func mySleepV2(timeout time.Duration) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout) // задаем продолжительность работы
	defer cancel()
	func(ctx context.Context) {
		select {
		case <-ctx.Done(): // ждет завершения контекста
			return // завершает функцию
		}
	}(ctx)
}

// функция сна с помощью time.Timer
func mySleepV3(timeout time.Duration) {
	timer := time.NewTimer(timeout) // создаем таймер на определенное время
	<-timer.C                       // после окончания таймера в канал C будет отправленно текущее время
}

// функция сна с помощью time.Ticker
func mySleepV4(timeout time.Duration) {
	ticker := time.NewTicker(timeout) // создаем тикер на определенное время
	<-ticker.C                        // после окончания тикер в канал C будет отправленно текущее время
	ticker.Stop()                     // останавливаем тикер, чтобы больше не отправлял данные
}

func timer(start time.Time) {
	fmt.Println("I slept for", time.Now().Sub(start))
}

func Task25() {
	fmt.Println("I am going to sleep")
	defer timer(time.Now())
	mySleepV1(1 * time.Second)
	mySleepV2(1 * time.Second)
	mySleepV3(1 * time.Second)
	mySleepV4(1 * time.Second)
}
