package solutions

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

// config, nothing interesting here
func config8() (uint64, int) {
	wc := uint64(12)
	k := 2
	if len(os.Args) > 3 {
		if buf, err := strconv.Atoi(os.Args[2]); err != nil {
			log.Printf("Task 4\nThe second argument is invalid.\n Got an error: %s\n Using default value: %d\n\n", err, wc)
		} else {
			wc = uint64(buf)
		}

		if buf, err := strconv.Atoi(os.Args[3]); err != nil {
			log.Printf("Task 4\nThe third argument is invalid.\n Got an error: %s\n Using default value: %d\n\n", err, wc)
		} else {
			k = buf
		}
	} else {
		fmt.Print("Insert int64: ")
		fmt.Scan(&wc)
		fmt.Print("Insert k: ")
		fmt.Scan(&k)
	}
	return wc, k
}

func doTask8(num uint64, k int) uint64 {
	mask := uint64(1) << k // mask for <or> operation
	res := num | mask      // trying to change a byte
	if res == num {        // if nothing has changed
		res = num &^ mask // making mask for <and> operation and changing a byte
	}
	return res
}

func Task8() {
	num, k := config8()

	fmt.Printf("origin: %b\n", num)
	fmt.Printf("result: %b\n", doTask8(num, k))
}
