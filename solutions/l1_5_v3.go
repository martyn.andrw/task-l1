package solutions

import (
	"fmt"
	"time"
)

// killing parent goroutine
func writerTV3(ch chan<- struct{}) {
	for {
		ch <- struct{}{}                   // writes something to channel
		time.Sleep(100 * time.Millisecond) // not so fast
	}
}

func readerTV3(ch <-chan struct{}) {
	for {
		<-ch // reads something
		fmt.Println("reader read something")
	}
}

func Task5V3() {
	N := configTime()
	ch := make(chan struct{})

	go writerTV2(ch, time.After(N*time.Second))
	go readerTV2(ch, time.After(N*time.Second))
	time.Sleep(N * time.Second)
	// after sleep, parent goroutine (Task5V3) will complete
	// its execution, which will complete child execution
}
