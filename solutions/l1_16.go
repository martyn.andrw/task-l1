package solutions

import (
	"fmt"
	"math/rand"

	"golang.org/x/exp/constraints"
)

type RealNums interface {
	constraints.Integer | constraints.Float
}

func partition[T RealNums](arr []T) int {
	l := 0
	r := len(arr) - 1

	pivot := arr[0] // выбираем опорный элемент, в данном случае выбран нулевой
	for l < r {
		for ; l < len(arr) && arr[l] <= pivot; l++ { // ищем первый элемент меньший выбранного
		}
		for ; r >= 0 && arr[r] > pivot; r-- { // ищем первый элемент больший выбранного
		}
		if l < r { // если левый элемент левее правого, то меняем их местами
			arr[l], arr[r] = arr[r], arr[l]
		}
	}
	arr[0], arr[r] = arr[r], arr[0] // переносим опорный элемент в конец меньших ему элементов
	return r                        // возвращаем позицию опорного элемента
}

func quickSort[T RealNums](arr []T) {
	if len(arr) > 1 {
		pivot := partition(arr)
		quickSort(arr[:pivot])   // так как все элементы меньшие опорного эл. находятся левее,
		quickSort(arr[pivot+1:]) // а все большие правее, то нет смысла оп.эл. участвовать в дальнейшей сортировке
	}
}

func randomArr[T RealNums](size int) []T {
	arr := make([]T, 0, size)
	for ; size > 0; size-- {
		arr = append(arr, T(rand.Intn(size)))
	}
	return arr
}

func Task16() {
	arr := []int{9, 2, 3, 4, 51, 12, 5, 6}
	quickSort(arr)
	fmt.Println(arr)
	fmt.Println()

	arr = []int{9, 2, 3, 4, 51, 12, 2, 6}
	quickSort(arr)
	fmt.Println(arr)
	fmt.Println()

	arrf := []float32{9.3, 2.1, 3.0, 4, 51.22, 12.34, 2.86, 6}
	quickSort(arrf)
	fmt.Println(arrf)
	fmt.Println()

	arrf = randomArr[float32](100)
	quickSort(arrf)
	fmt.Println(arrf)
	fmt.Println()
}
