package solutions

import (
	"fmt"
	"math"
)

// структура поинт с инк. параметрами
type Point struct {
	x, y float64
}

// "конструктор"
func NewPoint(x, y float64) *Point {
	var point Point
	point.x, point.y = x, y
	return &point
}

// без геттеров инкапсулированные параметры не достать
func (p Point) X() float64 {
	return p.x
}

func (p Point) Y() float64 {
	return p.y
}

// функция нахождения дистанции, которая может находиться в другом пакете
func GetDistance(left, right Point) float64 {
	x := left.X() - right.X()
	y := left.Y() - right.Y()

	return math.Sqrt(x*x + y*y)
}

func Task24() {
	fmt.Println(GetDistance(*NewPoint(2, 3), *NewPoint(6, 3)))
	fmt.Println(GetDistance(*NewPoint(0, 0), *NewPoint(3, 4)))
	fmt.Println(GetDistance(*NewPoint(-5, 1), *NewPoint(3, 4)))
}
