package solutions

import (
	"fmt"
	"math/big"
)

func Task22() {
	// без изобретения велосипеда
	smt1 := new(big.Int)
	smt2 := new(big.Int)
	smt3 := new(big.Int)
	smt1.SetString("110000000000000000000000000", 10)
	smt2.SetString("110000000000000000000000000", 10)

	fmt.Println(smt3.Add(smt1, smt2))
	fmt.Println(smt3.Sub(smt1, smt2))
	fmt.Println(smt3.Mul(smt1, smt2))
	fmt.Println(smt3.Div(smt1, smt2))
}
