package solutions

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

// using context and sleep(can be replaced with waitgroup)
func workerV2(ctx context.Context, ch <-chan int, num int) {
	for {
		select {
		case <-ctx.Done(): // waiting for context to complete
			fmt.Printf("Worker %d got interrupt signal.\n", num)
			return
		case el := <-ch: // reading from channel
			fmt.Printf("Worker %d got: %d\n", num, el)
		}
	}
}

func writerV2(ctx context.Context, ch chan<- int) {
	for {
		select {
		case <-ctx.Done(): // waiting for context to complete
			fmt.Printf("Writer got interrupt signal.\n")
			close(ch)
			return
		case ch <- rand.Intn(100): // writing random num to channel
			time.Sleep(100 * time.Millisecond) // not so fast
		}
	}
}

func Task4V2() {
	wc := configCount()  // getting count of workers
	ch := make(chan int) // channel for communication

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt) // interrupt context
	defer cancel()

	go writerV2(ctx, ch)
	for i := wc; i > 0; i-- {
		go workerV2(ctx, ch, i)
	}
	<-ctx.Done()                       // waiting for context to complete
	time.Sleep(100 * time.Millisecond) // waiting for writer to awake
} // we can use sync.waitgroup, but i want to show another way to wait
