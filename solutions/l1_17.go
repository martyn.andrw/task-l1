package solutions

import "fmt"

func binSearch[T RealNums](arr []T, el T) int {
	res := -1
	ind := len(arr) / 2 // начинаем с середины
	if len(arr) == 0 {
		return res
	}

	if el < arr[ind] { // если искомый элемент меньше срединного,
		res = binSearch(arr[:ind], el) // то ищем в левой части слайса
	}
	if el > arr[ind] { // если искомый элемент больше срединного,
		res = binSearch(arr[ind+1:], el) // то ищем в правой части слайса
		if res > -1 {                    // если нашли, тогда поправляем индекс
			res += ind + 1 // так как отправляли не весь слайс, то и индексы смещенный
		}
	}
	if el == arr[ind] { // если искомый равен срединному,
		res = ind // то возвращаем его индекс
	}

	return res
}

func Task17() {
	arr := []int{9, 2, 3, 4, 51, 12, 5, 6}
	quickSort(arr) // binary search requires a sorted array
	ind := 1
	el := arr[ind]
	fmt.Printf("array: %v, trying to find: %v, index: %d\n", arr, el, ind)
	fmt.Println(binSearch(arr, el))
	fmt.Println()

	arr = []int{9, 2, 3, 4, 51, 12, 2, 6}
	quickSort(arr)
	ind = 3
	el = arr[ind]
	fmt.Printf("array: %v, trying to find: %v, index: %d\n", arr, el, ind)
	fmt.Println(binSearch(arr, el))
	fmt.Println()

	arrf := []float32{9.3, 2.1, 3.0, 4, 51.22, 12.34, 2.86, 6}
	quickSort(arrf)
	indf := 7
	elf := arrf[indf]
	fmt.Printf("array: %v, trying to find: %v, index: %d\n", arrf, elf, indf)
	fmt.Println(binSearch(arrf, elf))
	fmt.Println()
	fmt.Println()

	el = 100
	fmt.Printf("array: %v, trying to find: %v, index: %d\n", arr, el, -1)
	fmt.Println(binSearch(arr, el))
	fmt.Println()

	elf = 100
	fmt.Printf("array: %v, trying to find: %v, index: %d\n", arrf, elf, -1)
	fmt.Println(binSearch(arrf, elf))
	fmt.Println()
}
