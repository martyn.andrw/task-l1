package solutions

import (
	"fmt"
	"math/rand"
)

// using constraint from past
// type Computable interface {
// 	constraints.Integer |
//	constraints.Float |
//	constraints.Complex
// }

// creates new channel and writes nums from array to it
func writer[T Computable](nums []T) <-chan T {
	out := make(chan T) // new channel
	go func() {         // another goroutine writes nums to channel
		for _, num := range nums { // from array
			out <- num
		}
		close(out) // when all numbers are written to the channel, we close it
	}()
	return out
}

// reads from channel, squres and writes results to another channel
func sqr[T Computable](ch <-chan T) <-chan T {
	out := make(chan T) // new channel
	go func() {         // another g that writes squares to output channel
		for num := range ch { // reads until numbers have been read and channel closes
			out <- num * num
		}
		close(out) // when all numbers are written to the channel, we close it
	}()
	return out
}

// just random generation for tests
func randomAI(size int) []int {
	res := make([]int, 0, size)
	for i := 0; i < size; i++ {
		res = append(res, rand.Intn(200))
	}
	return res
}

func Task9() {
	arri := randomAI(10)
	for el := range sqr(writer(arri)) {
		fmt.Printf("%v ", el)
	}
	fmt.Println()
}
