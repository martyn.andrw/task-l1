package solutions

import "fmt"

// the math way
func swapMath[T Computable](left, right T) (T, T) {
	left += right // there may be another math operation here
	right = left - right
	left = left - right
	return left, right
}

// the normal way
func swapAny[T any](left, right T) (T, T) {
	return right, left
}

// almost normal way
func swapAnyV1[T any](left, right T) (T, T) {
	left, right = right, left
	return left, right
}

func Task13() {
	fmt.Println(swapMath(1, 2))
	fmt.Println(swapMath(1.0, 2.5))
	fmt.Println(swapMath(0, 0))
	fmt.Println(swapMath(5+2i, 8-2i))
	fmt.Println()

	fmt.Println(swapAny("abc", "cba"))
	fmt.Println(swapAny([]int{1, 2, 3}, []int{3, 2, 1}))
	fmt.Println()

	fmt.Println(swapAny("abc", "cba"))
	fmt.Println(swapAny([]int{1, 2, 3}, []int{3, 2, 1}))
}
