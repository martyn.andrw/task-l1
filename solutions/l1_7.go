package solutions

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
)

type syncMap[K comparable, V any] struct {
	data map[K]V
	mu   sync.RWMutex
}

func (sm *syncMap[K, V]) writeToMap(key K, value V) (err error) {
	sm.mu.Lock() // read/write lock
	_, exists := sm.data[key]
	if exists { // if exist returns error
		err = errors.New(fmt.Sprintf("%v already exists", key))
	} else {
		sm.data[key] = value
	}
	sm.mu.Unlock() // release
	return
}

func (sm *syncMap[K, V]) readFromMap(key K) (value V, err error) {
	sm.mu.RLock() // write lock
	value, exists := sm.data[key]
	if !exists { // if not exists return zerovalue and error
		err = errors.New(fmt.Sprintf("%v not exists", key))
	}
	sm.mu.RUnlock() // release
	return
}

// constructor
func newSyncMap[K comparable, V any]() syncMap[K, V] {
	return syncMap[K, V]{data: make(map[K]V)}
}

func Task7() {
	sm := newSyncMap[string, int]()
	var wg7 sync.WaitGroup
	wg7.Add(100) // adds all g at once

	for i := 0; i < 100; i++ {
		go func() {
			err := sm.writeToMap("abc", rand.Intn(10))
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println(sm.readFromMap("abc"))
			wg7.Done()
		}()
	}
	wg7.Wait() // waits all g
}
