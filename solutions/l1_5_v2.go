package solutions

import (
	"fmt"
	"time"
)

// using channels
func writerTV2(ch chan<- struct{}, timeout <-chan time.Time) {
	for {
		select {
		case <-timeout: // waits until timeout
			close(ch) // closes channel
			return    // and ends g
		default:
			ch <- struct{}{}                   // writes to channel
			time.Sleep(100 * time.Millisecond) // not so fast
		}
	}
}

func readerTV2(ch <-chan struct{}, timeout <-chan time.Time) {
	for {
		select {
		case <-timeout: // waits until timeout
			return // ends g
		case <-ch: // reads from channel
			fmt.Println("reader read something")
		}
	}
}

func Task5V2() {
	N := configTime()
	ch := make(chan struct{})

	go writerTV2(ch, time.After(N*time.Second)) // when the timeout expires,
	go readerTV2(ch, time.After(N*time.Second)) // the current time will be sent to the channel

	<-time.After(N * time.Second) // waits until timeout
}
