package solutions

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// the mutex way
type counterMU struct {
	count int
	mu    sync.RWMutex
}

func (c *counterMU) Increment(inc int) {
	c.mu.Lock() // блокируем на чтение/запись
	c.count += inc
	c.mu.Unlock() // освобождаем
}

func (c *counterMU) Get() int {
	c.mu.RLock() // блокируем на запись. (чтение не блокируется)
	res := c.count
	c.mu.RUnlock() // освобождаем
	return res
}

// the atomic way
type counterAT struct {
	count int32
}

// atomic работает на уровне процессора,
// поэтому он может выполнять только одну операцию
func (c *counterAT) Increment(inc int32) {
	atomic.AddInt32(&c.count, inc)
}

func (c *counterAT) Get() int32 {
	return atomic.LoadInt32(&c.count)
}

func Task18() {
	var counterm counterMU
	var countera counterAT

	var wg sync.WaitGroup
	wg.Add(200)

	for i := 0; i < 100; i++ {
		go func() {
			counterm.Increment(1)
			wg.Done()
		}()
		go func() {
			countera.Increment(1)
			wg.Done()
		}()
	}

	wg.Wait()
	fmt.Println(counterm.Get())
	fmt.Println(countera.Get())
}
