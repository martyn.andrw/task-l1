package solutions

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// kill main ¯\_( ͡❛ ㅅ ͡❛)_/¯
func g6v1() {
	for {
		fmt.Println("gv1 is still alive")  // notifies that v1 is alive
		time.Sleep(300 * time.Millisecond) // not so fast
	}
}

var wg6 sync.WaitGroup // waitgroup to wait output from v2, v3, v4

// until context is complete
func g6v2(ctx context.Context) {
	for {
		select {
		case <-ctx.Done(): // waits until context is done
			fmt.Println("gv2 done") // Also context can be with timeout or deadline
			wg6.Done()              // notifies wg that this g has ended
			return
		default:
			fmt.Println("gv2 is still alive")  // still alive
			time.Sleep(300 * time.Millisecond) // not so fast
		}
	}
}

// using range channel
func g6v3(ch chan struct{}) {
	for el := range ch { // until channel is close
		fmt.Println("gv3 is still alive")  // g is alive
		time.Sleep(300 * time.Millisecond) // not so fast
		func() {
			c := el // wtf
			el = c
		}()
	}
	fmt.Println("gv3 done") // notifies us that this g has ended
	wg6.Done()              // notifies wg that this g has ended
}

// using additional channel
func g6v4(ch chan struct{}, quit chan struct{}) {
	for {
		select { // same as switch but for channel
		default:
			fmt.Println("g6v4 is still alive") // bla bla bla
			time.Sleep(300 * time.Millisecond) // not so fast
		case <-quit:
			fmt.Println("g6v4 done") // notifies us that this g has ended
			wg6.Done()               // notifies wg that this g has ended
			return
		}
	}
}

func Task6() {
	ctx, cancel := context.WithCancel(context.Background())
	ch := make(chan struct{})
	quit := make(chan struct{})

	wg6.Add(3) // doesn't wait for gv1, b'cos v1 will end when main ends
	go g6v1()
	go g6v2(ctx)
	go g6v3(ch)
	go g6v4(ch, quit)

	time.Sleep(2 * time.Second)
	cancel()           // completes context
	close(ch)          // closes channel
	quit <- struct{}{} // writes to additional channel
	wg6.Wait()         // waits almost all goroutines
}
