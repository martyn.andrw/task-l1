package solutions

import (
	"fmt"
	"strings"
)

func reverseSentence(str string) string {
	// здесь использую разбиение строки по строке
	words := strings.Split(str, " ")
	size := len(words)

	// идем до середины и меняемся с концом
	for i := 0; i < size/2; i++ {
		words[i], words[size-i-1] = words[size-i-1], words[i]
	}

	// кладем все в string.Builder для конкатенации
	var res strings.Builder
	for i, word := range words {
		res.WriteString(word)
		if i < size-1 {
			res.WriteString(" ")
		}
	}

	return res.String()
}

func testrs20(str string) {
	fmt.Printf("%s : %s\n", str, reverseSentence(str))
}

func Task20() {
	testrs20("光栄の へナイラ ック")
	testrs20("глав рыба")
	testrs20("топот")

	testrs20("")
	testrs20("a")
	testrs20("ab ")
	testrs20("ab a")
}
