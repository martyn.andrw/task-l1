package solutions

import "fmt"

type geter interface {
	get() int
}

// some structure
type someStruct struct {
	val int
}

// some method not satisfying the interface <geter>
func (s someStruct) Val() int {
	return s.val
}

// adapter
type adapterStruct struct {
	someStruct someStruct
}

// methon satisfying the interface <geter>
// and using non-satisfying method
func (a adapterStruct) get() int {
	return a.someStruct.Val()
}

// function for test
// if struct doesn't satisfy interface will be compilation error
func someFuncAd(adapter geter) {
	fmt.Printf("%T: %v\n", adapter, adapter.get())
}

func Task21() {
	var some someStruct
	var adapter adapterStruct

	some.val = 13
	adapter.someStruct = some

	someFuncAd(adapter)
}
