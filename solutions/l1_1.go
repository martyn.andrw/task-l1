package solutions

import "fmt"

// комментарии на английском, потому что неудобно переключать раскладку

// struct Human has 3 fields
type Human struct {
	FirstName string
	LastName  string
	Age       int
}

// and 1 method
func (h *Human) SayName() {
	fmt.Printf("Hello world! My name is %s %s. I am %d years old.\n", h.FirstName, h.LastName, h.Age)
}

// the first way
type Action struct {
	Human
}

// the second way
type ActionV1 struct {
	Human Human
}

func (a ActionV1) SayName() {
	a.Human.SayName()
}

func Task1() {
	human := Human{FirstName: "Friedrich", LastName: "Nietzsche", Age: 38}
	human.SayName()

	fmt.Print("\n\n")

	action := Action{Human{FirstName: "Franz", LastName: "Kafka", Age: 29}}
	// we have 2 ways to get our method:
	action.SayName()       // from child
	action.Human.SayName() // from parent

	fmt.Print("\n\n")

	actionV1 := ActionV1{Human{FirstName: "Immanuel", LastName: "Kant", Age: 24}}
	// we have 2 ways to get our method just like the previous time.
	// but now with one additional method in child struct
	actionV1.SayName()       // additional method
	actionV1.Human.SayName() // original method
}
