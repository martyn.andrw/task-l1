package solutions

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

// config for task 5
func configTime() time.Duration {
	sec := 2
	if len(os.Args) > 2 {
		if buf, err := strconv.Atoi(os.Args[2]); err != nil {
			log.Printf("Task 4\nThe second argument is invalid.\n Got an error: %s\n Using default value: %d\n\n", err, sec)
		} else {
			sec = buf
		}
	} else {
		fmt.Print("Insert count of seconds: ")
		fmt.Scan(&sec)
	}
	return time.Duration(sec)
}
