package solutions

import (
	"fmt"
	"math/rand"

	"golang.org/x/exp/slices"
)

func createHugeString(size int64) string {
	bytes := make([]byte, 0, size)

	for i := int64(0); i < size; i++ {
		bytes = append(bytes, byte(rand.Intn(128)))
	}

	return string(bytes)
}

var justString string

// строка это массив байтов, при попытке взять слайс можем получить
// проблемы с символами, которые занимают более одного байта,
// также если берем слайс от большой строки, мы будем таскать
// большую строку с собой в качестве базового массива для слайса
func someFunc(str string) string {
	return str[:100]
}

// в данной функции создается новый слайс с новым базовым массивом
// в этот массив копируются значения из большой строки
func someFuncRightVersion(str string) string {
	arr := slices.Clone([]rune(str)[:100])
	return string(arr) //
}

func Task15() {
	str := createHugeString(1 << 10)
	fmt.Println(someFunc(str))
	fmt.Println(someFuncRightVersion(str))

	// fmt.Println(someFunc("ジョジョの奇妙な冒険"))
	// fmt.Println(someFuncRightVersion("ジョジョの奇妙な冒険"))
}
