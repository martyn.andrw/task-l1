package solutions

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

// just sets args
func configCount() int {
	wc := 8
	if len(os.Args) > 2 {
		if buf, err := strconv.Atoi(os.Args[2]); err != nil {
			log.Printf("Task 4\nThe second argument is invalid.\n Got an error: %s\n Using default value: %d\n\n", err, wc)
		} else {
			wc = buf
		}
	} else {
		fmt.Print("Insert count of workers: ")
		fmt.Scan(&wc)
	}
	return wc
}
