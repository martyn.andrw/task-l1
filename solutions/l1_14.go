package solutions

import (
	"fmt"
	"reflect"
)

type mmm struct {
	a int
	b float32
}

func Task14() {
	// first way
	// using string formatting or print formatting
	var x0 interface{} = make(chan int)
	fmt.Print(fmt.Sprintf("%T", x0))
	fmt.Printf("\t%T\n", x0)

	// second way
	// using reflect
	var x1 interface{} = make(map[string]int)
	fmt.Println(reflect.TypeOf(x1))

	// third way
	// using type switch
	var x2 interface{} = string("hello")
	switch x2.(type) {
	case int:
		fmt.Println("int")
	case float32:
		fmt.Println("float32")
	case string:
		fmt.Println("string")
		// case etc
	default:
		fmt.Println("need to add this type to switch-case")
	}

	// fourth way
	// shouldn't be used
	fmt.Println(x2.(string)) // if you guessed right will be ok
	// fmt.Println(x2.(int))    // if not will be panic
}
