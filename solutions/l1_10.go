package solutions

import "fmt"

// в Go нет структуры set
// в качестве set использую map,
// потому что один и тот же ключ может быть добавлен в нее лишь единожды,
// плюс вставка и получение работает за О(1).
// при хрании значений в key нужно что-то класть в value
// лучшим решение будет хранение пустой структуры, занимающей 0 байт
func doGroups(temperatures ...float32) map[int]map[float32]struct{} {
	tm := make(map[int]map[float32]struct{})

	for _, t := range temperatures {
		temp := int(t/10) * 10
		if _, exists := tm[temp]; !exists { // если группа не существует,
			tm[temp] = make(map[float32]struct{}) // тогда создаем "подмножество"
			tm[temp][t] = struct{}{}              // и вставляем "значения" в "множество"
		} else { // иначе пробуем вставить "значение" в "множество"
			tm[temp][t] = struct{}{} // в качестве значения в мапу используем пустую структуру
		}
	}

	return tm
}

// just clean output
func grprintln(m map[int]map[float32]struct{}) {
	for gr, mapTemp := range m {
		arrt := make([]float32, 0, len(mapTemp))
		for temp := range mapTemp {
			arrt = append(arrt, temp)
		}
		fmt.Printf("%d:%v ", gr, arrt)
	}
	fmt.Println()
}

func Task10() {
	mt := doGroups(-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5)
	grprintln(mt)
}
