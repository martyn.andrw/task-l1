package solutions

import (
	"fmt"
	"math/rand"
	"time"
)

// just kill main goroutine which will kill all others
func workerV3(ch <-chan int, num int) {
	for {
		el := <-ch // reads from channel
		fmt.Printf("Worker %d got: %d\n", num, el)
	}
}

func writerV3(ch chan<- int) {
	for {
		ch <- rand.Intn(100)               // writes to channel
		time.Sleep(100 * time.Millisecond) // not so fast
	}
}

func Task4V3() {
	wc := configCount()  // count of workers
	ch := make(chan int) // new channel for data

	go writerV3(ch) // starts writer in another g
	for i := wc; i > 0; i-- {
		go workerV3(ch, i) // starts reader in another g
	}

	for { // waiting for interrupt signal
	} // that will automaticly kill main
}
