package solutions

import (
	"context"
	"fmt"
	"time"
)

// using context
func writerTV1(ctx context.Context, ch chan<- struct{}) {
	for {
		select {
		case <-ctx.Done(): // waiting for context to complete
			return
		default:
			ch <- struct{}{}                   // writes to channel
			time.Sleep(100 * time.Millisecond) // not so fast
		}
	}
}

func readerTV1(ctx context.Context, ch <-chan struct{}) {
	for {
		select {
		case <-ctx.Done():
			return // if context is done, returns
		case <-ch: // reads from channel
			fmt.Println("reader read something")
		}
	}
}

func Task5V1() {
	N := configTime()
	ctx, cancel := context.WithTimeout(context.Background(), N*time.Second)
	ch := make(chan struct{})
	defer cancel()

	go writerTV1(ctx, ch)
	go readerTV1(ctx, ch)
	<-ctx.Done()
}
