package solutions

import "fmt"

// в Go нет структуры set
// создаю новую структуру для удобства использования.
// в качестве хранилища данных используется map,
// потому что один и тот же ключ может быть добавлен в нее лишь единожды,
// плюс вставка и получение работает за О(1).
// при хрании значений в key нужно что-то класть в value
// лучшим решение будет хранение пустой структуры, занимающей 0 байт
type set[V comparable] struct {
	data map[V]struct{}
}

// constructor
func newSet[V comparable]() set[V] {
	return set[V]{data: make(map[V]struct{})}
}

// printer
func (s set[V]) println() {
	fmt.Print("set[ ")
	for val := range s.data {
		fmt.Printf("%v ", val)
	}
	fmt.Print("]\n")
}

func (s set[V]) insert(values ...V) {
	for _, value := range values {
		s.data[value] = struct{}{}
		// как уже упоминалось ранее в поле value нужно что-то хранить,
		// поэтому храню пустую структуру
	}
}

func intersection[V comparable](left, right set[V]) set[V] {
	l := left
	r := right
	if len(left.data) > len(right.data) { // выполняется за константное время, так как мапа хранит size в отдельной переменной
		l = right // выбираю мапу с меньшим количеством элементов,
		r = left  // чтобы сократить время работы
	}

	res := newSet[V]()        // новый сет для возврата
	for val := range l.data { // проходимся в цикле по меньшей мапе
		if _, exists := r.data[val]; exists { // проверяем наличие в другой мапе
			res.insert(val) // в случае наличия в обеих мапах добавляем
		}
	}
	return res
}

func Task11() {
	s0 := newSet[int]()
	s0.insert(1, 2, 3, 3)
	s0.println()

	s1 := newSet[int]()
	s1.insert(0, 2, 4, 3)
	s1.println()

	s2 := intersection(s0, s1)
	s2.println()
}
