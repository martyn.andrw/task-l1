package solutions

import "fmt"

func deleteFromSlice[T any](arr []T, index int) []T {
	if index < 0 || index >= len(arr) {
		return arr
	}
	// возврат старого массива со скопированными элементами
	// на одну позицию влево.
	// Также будет изменен параметр len, cap - без изменений
	return append(arr[:index], arr[index+1:]...)
}

func testDeleteFS[T any](arr []T, ind int) {
	fmt.Printf("%v\n", deleteFromSlice(arr, ind))
}

func Task23() {
	testDeleteFS([]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 3)
	testDeleteFS([]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 0)
	testDeleteFS([]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, -1)
	testDeleteFS([]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, 10)
}
