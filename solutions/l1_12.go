package solutions

import "fmt"

// instead of set I used map[type]struct{}
func getSetString(strs ...string) map[string]struct{} {
	ms := make(map[string]struct{})

	// read from strs and writes in map keys
	for _, str := range strs {
		ms[str] = struct{}{} // instead of values I used struct{}
	}

	return ms
}

// just printer
func setStrPrintln(ms map[string]struct{}) {
	arr := make([]string, 0, len(ms))
	for val := range ms {
		arr = append(arr, val)
	}
	fmt.Printf("%v", arr)
	fmt.Println()
}

func Task12() {
	setStrPrintln(getSetString("cat", "cat", "dog", "cat", "tree"))
}
