package solutions

import (
	"fmt"
	"sync"

	"golang.org/x/exp/constraints"
)

// constraint for generic
type Computable interface {
	constraints.Integer |
		constraints.Float |
		constraints.Complex
}

// using waitgroup and pointer
func ConcurencySqrWGP[T Computable](array []T) {
	var wg sync.WaitGroup // closure
	wg.Add(len(array))
	for i := range array {
		go func(el *T) { // elements have their own memory cells
			*el *= *el // changing value by pointer
			wg.Done()  // notify that we are done here
		}(&array[i]) // send pointer to memory cell
	}
	wg.Wait() // waits for all goroutines
}

// using channel
func ConcurencySqrC[T Computable](array []T) {
	ch := make(chan []T, 1)                 // creates new channel for data
	quit := make(chan struct{}, len(array)) // creates new waiting channel

	ch <- array // sends array for first goroutine
	for i := range array {
		go func(i int, ch chan []T) {
			arr := <-ch        // takes array from data channel
			arr[i] *= arr[i]   // changes value
			ch <- arr          // sends array to channel for next g
			quit <- struct{}{} // notify that g is done
		}(i, ch)
	}

	for i := 0; i < len(array); i++ {
		<-quit // waits for all notifications from goroutines
	}
	<-ch // reads array from channel
}

// using mutex
func ConcurencySqrM[T Computable](array []T) {
	var wg sync.WaitGroup // closure
	var mu sync.Mutex     // init new mutex

	for i := range array {
		wg.Add(1) // add 1 g to waitgroup
		go func(array []T, i int) {
			mu.Lock()            // gives access to data only to this g
			array[i] *= array[i] // changing value
			mu.Unlock()          // releases
			wg.Done()            // notifies that g is done
		}(array, i)
	}
	wg.Wait() // waits all g from group
}

func Task2() {
	arri := []int{1, 2, 3, 4, 5, 6}
	arrf := []float32{1, 1.5, 1.1}
	arrc := []complex64{2i, 2, 4}

	ConcurencySqrWGP(arri)
	// ConcurencySqrWGP(arrf)
	// ConcurencySqrWGP(arrc)

	// ConcurencySqrM(arri)
	ConcurencySqrM(arrf)
	// ConcurencySqrM(arrc)

	// ConcurencySqrC(arri)
	// ConcurencySqrC(arrf)
	ConcurencySqrC(arrc)

	fmt.Println(arri)
	fmt.Println(arrf)
	fmt.Println(arrc)
}
